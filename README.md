# MESSAGING - Kafka Task

### Prerequisites
Configure a Kafka cluster using Docker with the following parameters: * Number of brokers - 3 * Number of partitions - 3 * Replication factor - 2 * observe the Kafka broker logs to see how leaders/replicas for every partition are assigned

### Practical task
1. Implement a pair of "at least once" producer and "at most once" consumer.
2. Implement another pair of producer and consumer with exactly-once delivery (use the Transactional API
3. Implement a taxi application using Spring Boot. The application should consist of three components
   1. REST service fo sending taxi coordinates and car ID
    1. Kafka broker
    2.  Three consumers to calculate the distance traveled by a car.

### How to validate the tasks
The tasks 1 and 2 can be validate executing the integrations tests of the application
```bash
mvn test
```
To valid the task 3 you should run the docker-compose.yml that is present in the root folder of this repository
```bash
docker-compose up
```
After that you can run the application in the IDE of you choice or using maven command line
```bash
mvn package -DskipTests && java -jar target/kafka-homework-0.0.1-SNAPSHOT.jar
```
Once the application is up and running you can run the following request and check the logs to see how many kilometers the taxi traveled
```bash
curl -X POST 'http://localhost:8080/taxi/1' \
--header 'Content-Type: application/json' \
--data-raw '{
  "latitude": 50.0541115,
  "longitude": 19.9332343
}'
```
Expected output in the logs
```log
INFO 1718023 --- [tracker-1-0-C-1] c.e.m.k.t.c.DistanceCalculatorConsumer1  : Taxi 2 traveled 0.00KM 
```
You can run the request on more time and check the travel progress
```bash
curl -X POST 'http://localhost:8080/taxi/2' \
   --header 'Content-Type: application/json' \
   --data-raw '{
     "latitude": 50.0503351,
     "longitude": 19.9596307
   }'
```
Expected output in the logs
```log
INFO 1718023 --- [tracker-1-0-C-1] c.e.m.k.t.c.DistanceCalculatorConsumer1  : Taxi 2 traveled 1.93KM