package com.epam.message.kafka;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(
    partitions = 1,
    brokerProperties = { 
      "listeners:PLAINTEXT://localhost:9092", 
      "port=9092",
      "transaction.state.log.replication.factor=1",
      "transaction.state.log.min.isr=1"
    }
)
@ActiveProfiles(profiles = { "test-task2" })
public class EmbeddedKafkaIntegrationTask2Test {

    @Autowired
    private ConsumerExactlyOnce consumer;

    @Autowired
    private KafkaProducerTask2 producer;

    @Value("${test.topic}")
    private String topic;

    @Test
    public void givenEmbeddedKafkaBroker_whenSendingWithSimpleTransactionalProducer_thenMessageReceived() 
      throws Exception {
        final var data = "Sending messge with exaclty-once delivery";
        producer.sendTransactional(topic, data);
        
        final var messageConsumed = consumer.getLatch().await(10, TimeUnit.SECONDS);
        assertTrue(messageConsumed);
        assertThat(consumer.getPayload(), containsString(data));
    }
    
}
