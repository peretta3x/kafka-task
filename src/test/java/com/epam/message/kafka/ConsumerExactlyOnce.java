package com.epam.message.kafka;

import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Profile("test-task2")
public class ConsumerExactlyOnce {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerAtMostOnce.class);

    private CountDownLatch latch = new CountDownLatch(1);
    private String payload;

    @Transactional
    @KafkaListener(topics = "${test.topic}")
    public void receiveTransactional(final ConsumerRecord<?, ?> consumerRecord) {
        LOGGER.info("received payload='{}'", consumerRecord.toString());
        payload = consumerRecord.toString();
        latch.countDown();
    }

    public void resetLatch() {
        latch = new CountDownLatch(1);
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public String getPayload() {
        return payload;
    }
}
