package com.epam.message.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Profile("test-task1")
public class KafkaProducerTask1 {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerTask1.class);
    private final KafkaTemplate<String, String> kafkaTemplate;

    public KafkaProducerTask1(
        final KafkaTemplate<String, String> kafkaTemplate
    ) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Transactional
    public void send(final String topic, final String payload) {
        LOGGER.info("sending payload='{}' to topic='{}'", payload, topic);
        kafkaTemplate.send(topic, payload);
    }
    
}
