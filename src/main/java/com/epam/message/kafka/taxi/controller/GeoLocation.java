package com.epam.message.kafka.taxi.controller;

public record GeoLocation(
    double latitude, 
    double longitude
) {
    
}
