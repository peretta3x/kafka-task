package com.epam.message.kafka.taxi;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.epam.message.kafka.taxi.controller.TaxiPosition;

@Component
public class TaxiTrackerService {
    
    private Map<Long, TaxiPosition> lastPositions = new ConcurrentHashMap<>();
    private Map<Long, Double> distanceByTaxi = new ConcurrentHashMap<>();

    public void setLastPosition(TaxiPosition taxiPosition) {
        this.lastPositions.put(taxiPosition.id(), taxiPosition);
    }

    public TaxiPosition getLastPosition(Long id) {
        return this.lastPositions.get(id);
    }

    public void addDistanceTravelled(Long id, Double distance) {
        this.distanceByTaxi.put(id, this.distanceByTaxi.getOrDefault(id, 0d) + distance);
    }

    public Double getTaxiTravelledDistance(Long id) {
        return this.distanceByTaxi.getOrDefault(id, 0d);
    }

}
