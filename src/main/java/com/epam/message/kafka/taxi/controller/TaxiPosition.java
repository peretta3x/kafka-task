package com.epam.message.kafka.taxi.controller;

public record TaxiPosition(
    Long id,
    GeoLocation geoLocation
) {

}
