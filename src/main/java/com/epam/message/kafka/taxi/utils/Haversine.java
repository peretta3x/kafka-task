package com.epam.message.kafka.taxi.utils;

public class Haversine {
    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    public static double haversine(double lat1, double lon1,
            double lat2, double lon2) {
        // distance between latitudes and longitudes
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);

        // convert to radians
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // apply formulae
        double a = Math.pow(Math.sin(dLat / 2), 2) +
                Math.pow(Math.sin(dLon / 2), 2) *
                        Math.cos(lat1) *
                        Math.cos(lat2);
        double rad = EARTH_RADIUS;
        double c = 2 * Math.asin(Math.sqrt(a));
        return rad * c;
    }

    // public static double haversin(double val) {
    //     return Math.pow(Math.sin(val / 2), 2);
    // }

    public static void main(String[] args) {
        var distance = haversine(54.687157, 25.279652, 50.0541115d,19.9332343d);
        System.out.println("%.2f".formatted(distance));
    }
}
