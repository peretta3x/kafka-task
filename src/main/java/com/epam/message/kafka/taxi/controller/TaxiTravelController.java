package com.epam.message.kafka.taxi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/taxi")
@Profile("dev")
public class TaxiTravelController {

    List<GeoLocation> geoLocations = new ArrayList<>();

    private final KafkaTemplate<String, Object> kafkaTemplate;
    
    @Value("${test.topic}")
    private String topic;

    public TaxiTravelController(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping("{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void addLocation(@PathVariable("id") Long taxiId, @RequestBody GeoLocation geoLocation) {
        System.out.println("tax id %s".formatted(taxiId));
        geoLocations.add(geoLocation);
        System.out.println(topic);
        kafkaTemplate.send(topic, taxiId.toString(), new TaxiPosition(taxiId, geoLocation));
        System.out.println("sent");
    }
//50.0503351,19.9596307
//50.0541115,19.9332343
}