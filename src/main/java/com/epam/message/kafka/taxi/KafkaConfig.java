package com.epam.message.kafka.taxi;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfig {
    
    private static final int NUMBER_OF_PARTITIONS = 3;
    private static final short NUMBER_OF_REPLICATIONS = 2;

    @Value("${test.topic}")
    private String topic;

    @Bean
    public NewTopic taxisTrack() {
        return new NewTopic(topic, NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }
}
