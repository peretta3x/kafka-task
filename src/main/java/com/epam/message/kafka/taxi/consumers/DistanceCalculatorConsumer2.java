package com.epam.message.kafka.taxi.consumers;

import java.util.Objects;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.epam.message.kafka.taxi.TaxiTrackerService;
import com.epam.message.kafka.taxi.controller.TaxiPosition;
import com.epam.message.kafka.taxi.utils.Haversine;

@Component
@Profile("dev")
public class DistanceCalculatorConsumer2 {

    private final Logger LOGGER = LoggerFactory.getLogger(DistanceCalculatorConsumer2.class);

    private final TaxiTrackerService trackerService;

    public DistanceCalculatorConsumer2(TaxiTrackerService trackerService) {
        this.trackerService = trackerService;
    }

    @KafkaListener(topics = "${test.topic}", groupId = "${spring.kafka.consumer.group-id}", id = "tracker-2")
    public void calculateNewDistance2(final ConsumerRecord<String, TaxiPosition> record) {
        LOGGER.info("Consumer details: %s".formatted(record.toString()));

        var taxiPosition = record.value();
        var id = taxiPosition.id();
        var lastPosition = trackerService.getLastPosition(id);
        var distance = 0d;

        if (Objects.isNull(lastPosition)) {
            trackerService.addDistanceTravelled(id, distance);
        } else {
            distance = Haversine.haversine(
                lastPosition.geoLocation().latitude(),
                lastPosition.geoLocation().longitude(),
                taxiPosition.geoLocation().latitude(),
                taxiPosition.geoLocation().longitude()
                );
            trackerService.addDistanceTravelled(id, distance);
        }

        trackerService.setLastPosition(taxiPosition);
        LOGGER.info("Taxi %s traveled %.2fKM ".formatted(id, trackerService.getTaxiTravelledDistance(id)));
    }
    
}
